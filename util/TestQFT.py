import tensorflow as tf

import sys
sys.path.append("../lib/")

from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *
from QFT import * 

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

p1 = QFTObject( 1, 0, tf.constant([ [1, 2, 3, 4] ], dtype = tf.complex128 ) )
p2 = QFTObject( 1, 0, tf.constant([ [3, 4, 5, 6] ], dtype = tf.complex128 ) )
#p2 = QFTObject( 2, 0, tf.constant([ [ [3, 4, 5, 6], [3, 4, 5, 6], [3, 4, 5, 6], [3, 4, 5, 6] ] ], dtype = tf.complex128 ) )

#p1 = QFTObject( 1, 0, Const([ [1, 2, 3, 4] ]) )
#p2 = QFTObject( 1, 0, Const([ [3, 4, 5, 6] ]) )

gamma = DiracGamma()
gamma5 = DiracGamma5()

print sess.run(p1.tensor)
print sess.run(p2.tensor)
print sess.run(gamma.tensor)

p = gamma5 % gamma

print sess.run(p.tensor)
print sess.run(tf.shape(p.tensor) )

pp = p2 - gamma*gamma5 - gamma5*gamma

print sess.run(pp.tensor)
print sess.run(tf.shape(pp.tensor) )

print Clebsch(2, -2, 2, 1, 1, -1)

p4 = LorentzVector(Vector(Invariant(0.), Invariant(0.), Invariant(0.)), Invariant(0.770))

#print sess.run(LorentzBoostTensor(p4).tensor)
#pol = PolarisationVectors(6, p4, 0.770)
#for i,p in enumerate(pol) : 
#  print i, sess.run(p.tensor)

#proj = BosonProjector(4, p4, 0.770)
#proj = FermionProjector(3, p4, 0.770)

pol = DiracSpinors(5, p4, 0.770)
m = None
for p in pol : 
  dm = (gamma*(gamma*p)) * ((p.bar()*gamma)*gamma)
#  dm = gamma*(gamma*p)
  if m == None : m = dm
  else : m += dm
m *= (1./(2.*0.770))

#m = gamma*proj*gamma

print sess.run(m.tensor), m.lorentzRank, m.spinorStructure
print sess.run(tf.shape(m.tensor))

#pol = DiracSpinors(3, p4, 0.770)
#for i,p in enumerate(pol) : 
#  print i, sess.run(p.tensor), p.spinorStructure
#  print i, sess.run(p.bar().tensor), p.bar().spinorStructure
#  print i, sess.run((p % p.bar()).tensor), (p % p.bar()).spinorStructure
