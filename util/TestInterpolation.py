import tensorflow as tf
import itertools
import numpy as np
from ROOT import TH2F, TRandom3, TCanvas

h = TH2F("h", "", 30, -3., 3., 30, -3., 3.)
r = TRandom3()
for i in range(1000000) : 
  h.Fill(r.Gaus()+1.0, r.Gaus()-1. )
#h.Fill(0.25, 0.25, 1)
#h.Fill(-0.25, -0.25, 1)
#h.Fill(-0.25, 0.25, -1)
#h.Fill(0.25, -0.25, -1)

fptype = tf.float64

img = [[0., 1., 2.], [1., 2., 3.], [2., 3., 4.]]
t = tf.constant(img, dtype = fptype)
sess = tf.Session()
print sess.run(t)
coord = tf.constant([[0., 0.], [0.7, 1.7], [2.0, 2.0], ], dtype = fptype)
print sess.run(coord)

def Interpolate(t, c) : 
  rank = len(t.get_shape())
  ind = tf.cast(tf.floor(c), tf.int32)
  t2 = tf.pad(t, rank*[[1,1]], 'SYMMETRIC')
  wts = []
  for vertex in itertools.product([0, 1], repeat = rank) : 
    ind2 = ind + tf.constant(vertex, dtype = tf.int32)
    weight = tf.reduce_prod(1. - tf.abs(c - tf.cast(ind2, dtype = fptype)), 1)
    wt = tf.gather_nd(t2, ind2+1)
    wts += [ weight*wt ]
  interp = tf.reduce_sum(tf.pack(wts), 0)
  return interp

class RootTH2Shape : 
  def __init__(self, hist) : 
    nx = hist.GetNbinsX()
    ny = hist.GetNbinsY()
    array = np.zeros( (nx, ny), dtype = float)
    self.limits = [ 
                    tf.constant( [ hist.GetXaxis().GetBinCenter(1),  hist.GetYaxis().GetBinCenter(1) ], dtype = fptype ), 
                    tf.constant( [ hist.GetXaxis().GetBinCenter(nx), hist.GetYaxis().GetBinCenter(ny) ], dtype = fptype ), 
                  ]
    for x in range(nx) : 
      for y in range(ny) : 
        array[x][y] = hist.GetBinContent(x+1, y+1)
        print x, y, array[x][y]
    print hist.GetXaxis().GetBinCenter(1), hist.GetYaxis().GetBinCenter(1)
    print hist.GetXaxis().GetBinCenter(nx), hist.GetYaxis().GetBinCenter(ny)
    print self.limits
    self.array = tf.constant(array, dtype = fptype)
    self.ns = tf.constant( [nx-1, ny-1], dtype = fptype )

  def shape(self, x) : 
    c = (x - self.limits[0])/(self.limits[1]-self.limits[0])*self.ns
    return Interpolate(self.array, c)

shape = RootTH2Shape( h )
nintx = 100
ninty = 100
x = (tf.constant( range(nintx), dtype = fptype))/(float(nintx-1))*(h.GetXaxis().GetXmax()-h.GetXaxis().GetXmin()) + h.GetXaxis().GetXmin()
y = (tf.constant( range(ninty), dtype = fptype))/(float(ninty-1))*(h.GetYaxis().GetXmax()-h.GetYaxis().GetXmin()) + h.GetYaxis().GetXmin()
px, py = tf.meshgrid(x, y)
px2 = tf.reshape(px, [-1] )
py2 = tf.reshape(py, [-1] )
p = tf.pack( [px2, py2], 1 )

print sess.run(x)

h2 = TH2F("h2", "", nintx, h.GetXaxis().GetXmin(), h.GetXaxis().GetXmax(), ninty, h.GetYaxis().GetXmin(), h.GetYaxis().GetXmax())

#s = sess.run(shape.shape(tf.transpose(p)))
s = sess.run(shape.shape(p))

for i in range(nintx) : 
  for j in range(ninty) : 
    h2.SetBinContent(i+1, j+1, s[nintx*j+i])

c = TCanvas("c", "", 800, 400)
c.Divide(2, 1)
c.cd(1)
h.Draw("zcol")
c.cd(2)
h2.Draw("zcol")
c.Update()
